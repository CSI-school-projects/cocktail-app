import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';
import CocktailList from '../components/CocktailList.js';

export default function HomeScreen({navigation}) {
    const [selectedCocktail , setSelectedCocktail] = useState(null);

    const updateSelectedCocktail = (cocktail) => {

        setSelectedCocktail(cocktail);
    }
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Liste des cocktails</Text>
            <CocktailList navigation={navigation}/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        margin: 10,
        marginTop: 50,
    },
});