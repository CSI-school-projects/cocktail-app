import axios from "axios";
import React, { useEffect, useState } from "react";
import { Image, FlatList, StyleSheet, Text, View, SafeAreaView, TouchableOpacity } from "react-native";
import Cocktail from "./Cocktail.js";

export default function CocktailList({navigation}) {
    const [cocktailList, setCocktailList] = useState([]);

    const getCocktailList = async() => {
        try {
            const response = await axios.get(`https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=Cocktail`);
            if(response.data) {
                console.log(response.data);
                const list = response.data.drinks;
                setCocktailList(list);
                console.log(list);
            } else {
                console.log("Error: No data");
                throw new Error("Error: No data");
            }
        } catch (error) {
            console.log("Error: " + error);
        }
    }

    useEffect(() => {
        getCocktailList();
    }, []);

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                data={cocktailList}
                renderItem={({item}) => (
                    <Cocktail name={item.strDrink} uri={item.strDrinkThumb} navigation={navigation}/>)
                }
                numColumns={2}
                keyExtractor={item => item.idDrink}
                contentContainerStyle={styles.list}
            />
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        padding: 10,
    },
});