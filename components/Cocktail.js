import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, SafeAreaView, Image, TouchableOpacity } from 'react-native';
import axios from "axios";

export default function Cocktail({idCocktail, name, uri, navigation}) {
    const [cocktail, setCocktail] = useState(null);

    const getCocktail = async() => {
        try {
            const response = await axios.get(`https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${idCocktail}`);
            if(response.data) {
                const cocktail = response.data.drinks[0];
                setCocktail(cocktail);
            } else {
                console.log("Error: No data");
                throw new Error("Error: No data");
            }
        } catch (error) {
            console.log("Error: " + error);
        }
    }

    useEffect(() => {
        getCocktail();
    }, []);

    return (
        <SafeAreaView style={styles.cocktailContainer}>
            <TouchableOpacity onPress={() => navigation.navigate('Detail', {idCocktail: idCocktail})}>
                <Text style={styles.title}>{name}</Text>
                <Image source={{uri: `${uri}/preview`}} style={styles.cardImg}/>
            </TouchableOpacity>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    cocktailContainer: {
        flex: 1,
        margin: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F2F2F2',
        borderRadius: 10,
        padding: 10,
        elevation: 5,
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    cardImg: {
        width: 100,
        height: 100,
        margin: 10,
        borderRadius: 10,
    },
});